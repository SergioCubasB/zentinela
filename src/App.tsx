import { motion } from 'framer-motion'

import { Link } from 'react-router-dom'
import Styles from './Style.module.scss'
import { IoIosArrowRoundForward } from 'react-icons/io'
import { RiPlayMiniFill } from 'react-icons/ri'
import { AiOutlineWhatsApp } from 'react-icons/ai'
import { FiInstagram } from 'react-icons/fi'
import { BiLogoTiktok } from 'react-icons/bi'
import { FaTwitch } from 'react-icons/fa'
import { useState } from 'react'
import { ModalState } from './components/ModalState/ModalState'
import { AnimatePresence } from 'framer-motion'

function App() {
  const { container, containerHeader, containerBanner, containerBannerCounter, containerDesafio, containerPresentacion, containerHace,
    containerPhone, containerPhoneActions, containerParte,
    copy, logo, detail, social, socialActions, containerApps, containerAppsCard, containerComunidad, containerComunidadDescription, containerComunidadComment,
    containerComentarios, containerFamilia, containerParteCards, containerHeaderBurger, caracteristicas } = Styles

    const DATA = [
      [
        {
          left: [
            {
              title: 'Expande tu Red de Clientes',
              description: 'Conecta automáticamente con una creciente red de padres que buscan servicios confiables. Incrementa tu clientela sin esfuerzo adicional.'
            },
            {
              title: 'Expande tu Red de Clientes',
              description: 'Conecta automáticamente con una creciente red de padres que buscan servicios confiables. Incrementa tu clientela sin esfuerzo adicional.'
            },
            {
              title: 'Descuentos Exclusivos para Ti',
              description: 'Benefíciate de descuentos y alianzas con marcas líderes, optimizando tus gastos y mejorando tu experiencia diaria.'
            }
          ]
        },
        {
          rigth: [
            {
              title: 'Pago Eficiente y Garantizado',
              description: 'Disfruta de pagos puntuales y elimina la incertidumbre financiera al gestionar eficientemente los pagos de los padres.'
            },
            {
              title: 'Comunicación Simplificada en Emergencia',
              description: 'Ante retrasos o incidentes, informa padres y profesores con un clic, manteniendo a todos al tanto y priorizando la seguridad de los niños.'
            },
            {
              title: 'Eleva la Calidad de tu Servicio',
              description: 'Transparencia y comunicación constante generan confianza. Ofrece excelencia y fomenta compromiso para lealtad cliente.'
            }
          ]
        }
      ],
      [
        {
          left: [
            {
              title: 'Empadronamiento Eficiente',
              description: 'Registro simple y rápido de conductores.'
            },
            {
              title: 'Optimización de Rutas',
              description: 'Reducción significativa de tiempos de traslado.'
            }
          ]
        },
        {
          rigth: [
            {
              title: 'Seguridad para Padres',
              description: 'Verificación rigurosa de todas las personas que recogen a los estudiantes.'
            }
          ]
        }
      ]
    ]

    const [isType, setisType] = useState({
      id: 1,
      state: false
    })

    const toggleType = (id: number) => {
      if (id === 0) {
        return setisType({id, state: false})
      }
      
      setisType({id, state: true})
    }

  return (
    <div className={container}>
      <header className={containerHeader}>
        <img className='animate__animated animate__slideInLeft' src="/images/logo.svg" alt="" />

        <div className={containerHeaderBurger}>
          <input type="checkbox" />

          <span></span>
          <span></span>
          <span></span>
        </div>
        <div>
          <ul>
            <li className='animate__animated animate__zoomIn'>
              <Link to={''}>Nuestra solución</Link>
            </li>
            <li className='animate__animated animate__zoomIn'>
              <Link to={''}>Lo que ofrecemos</Link>
            </li>
            <li className='animate__animated animate__zoomIn'>
              <Link to={''}>Como funciona</Link>
            </li>
          </ul>

          <ul>
            <li className='animate__animated animate__zoomIn'>
              <Link to={''}>Iniciar sesión</Link>
            </li>
            <li>
              <button>Registrate</button>
            </li>
          </ul>
        </div>
      </header>

      <div className={containerBanner}>
        <div>
          <h1 className='animate__animated animate__bounce'>
            Conectando <strong>Seguridad</strong> y <strong>Confianza</strong>: Una Solución de Movilidad Escolar
          </h1>
          <p>
            ¿Preocupado por el viaje escolar de tus hijos? Con <span>Zentinela</span>, tienes control en tiempo real. Seguridad, flexibilidad en pagos y una experiencia personalizada.

            <br />
            <br />
            <strong>¡Descubre la tranquilidad ahora!</strong>
          </p>
          <button className='animate__animated animate__shakeX animate__delay-2s animate__repeat-2'>Registra a tu hijo hoy <span><IoIosArrowRoundForward /></span></button>

          <div className={containerBannerCounter}>
            <p>
              <strong>20k+</strong>
              Autos seguros
            </p>
            <p>
              <strong>10k+</strong>
              Estudiantes seguros
            </p>

          </div>
        </div>

        <div>
          <img src="/images/banner.svg" alt="" />

          <div>
            <button>
              <img src="/icons/google_play.svg" alt="" />
            </button>
            <button>
              <img src="/icons/apple_store.svg" alt="" />
            </button>
          </div>
        </div>
      </div>

      <div className={containerDesafio}>
        <div>
          <img src="/images/desafio.svg" alt="" />
          <button>
            <svg viewBox='0 0 200 200' width='200' height='200' xmlns='http://www.w3.org/2000/svg' className="link__svg" aria-labelledby="link1-title link1-desc">
              <title id="link1-title">Nuestra solucion •</title>
              <path id="link-circle" className="link__path" d="M 20, 100 a 80,80 0 1,1 160,0 a 80,80 0 1,1 -160,0" stroke="none" fill="none" />
              <path className="link__arrow" d="M 75 100 L 125 100  110 85 M 125 100 L 110 115"  fill="none"
              style={{
                transform: 'rotate(180deg)',
                transformOrigin: '50% 50%'
              }}/>
              <text className="link__text">
                <textPath href="#link-circle" stroke="none">
                  Nuestra solucion • Nuestra solucion •
                </textPath>
              </text>
            </svg>
          </button>
        </div>
        <div>
          <h2 className='animate__animated animate__bounce'>El desafío de la <span>movilidad</span> escolar <span>moderna</span></h2>
          <p>
          Cada día, muchos padres <strong>enfrentan la angustia</strong> de no saber cómo es el viaje de sus hijos al colegio: 
          <br />
          <br />
          <strong>¿Llegaron bien? ¿El transporte es seguro? 
          ¿Y si hay un imprevisto?</strong>
          </p>
        </div>
      </div>

      <div className={containerPresentacion}>
        <div>
          <h2> Te presentamos <span>Zentinela</span> </h2>
          <p>
            la herramienta que te devuelve el control y la tranquilidad. 
            <br />
            <br />
            Ahora puedes estar con tus hijos en cada paso de su trayecto escolar, incluso desde la distancia.
          </p>

          <div>
            <button>Comenzar  <span><IoIosArrowRoundForward /></span></button>
            <Link to={''}>Nuestras soluciones <span><IoIosArrowRoundForward /></span></Link>
          </div>
        </div>
        <div>
          <img src="/images/map.svg" alt="" />
        </div>
      </div>

      <div className={containerHace}>
        <h2>¿Qué hace a <span>Zentinela</span> única?</h2>

        <div>
          <ul>
            <li>
              Monitoreo en tiempo real <span></span>
            </li>
            <li>
              Selección de movilidades por dirección <span></span>
            </li>
            <li>
              Selección de asientos <span></span>
            </li>
            <li>
              <button>
                Ver video <span><RiPlayMiniFill /></span>
              </button>
            </li>
          </ul>

          <div className={containerPhone}>
            <img src="/images/phone.svg" alt="" />
            <div className={containerPhoneActions}>
              <button>
                <img src="/icons/google_play.svg" alt="" />
              </button>
              <button>
                <img src="/icons/apple_store.svg" alt="" />
              </button>
            </div>
          </div>

          <ul>
            <li>
              <span />Monitoreo en tiempo real 
            </li>
            <li>
              <span /> Selección de movilidades por dirección
            </li>
            <li>
              <span /> Selección de asientos
            </li>
            <li>
              <button>
                Ver video <span><RiPlayMiniFill /></span>
              </button>
            </li>
          </ul>

        </div>
      </div>

      <div className={containerParte}>
        <h2>¡Sé parte de la <span>revolución</span> en <br /> movilidad <span>escolar</span>!</h2>

        <div className={containerParteCards}>
          <div>
            <div style={{backgroundImage: 'url(/images/colegios.png)'}} />
            <button onClick={() => toggleType(1)}>Beneficios para colegios</button>
          </div>
          <div>
            <div style={{backgroundImage: 'url(/images/movilidad.png)'}} />
            <button onClick={() => toggleType(2)}>Beneficios para Movilidades</button>
          </div>
        </div>
      </div>

      <div className={containerComunidad}>
        <div className={containerComunidadComment}>
          <div>
            <p>La app de movilidad escolar es increíble.</p>
            <p><strong>Pablo González</strong></p>
          </div>
          <div>
            <p>Excelente herramienta en estos tiempos de tanta inseguridad.</p>
            <p><strong>Laura Martínez</strong></p>
          </div>
          <div>
            <p>Me siento más seguro con esta app.</p>
            <p><strong>Carlos Rodríguez</strong></p>
          </div>
        </div>

        <div className={containerComunidadDescription}>
          <h2>Únete a la <span>comunidad de padres</span> que han decidido <br /> dar un paso adelante en la <span>seguridad y<br /> comodidad</span> de sus hijos</h2>
          <button>Prueba Zentinela ahora <span><IoIosArrowRoundForward /></span></button>
        </div>

        <div className={containerComunidadComment}>
          <div>
            <p>Facilita la coordinación y la seguridad.</p>
            <p><strong>Ana Sánchez</strong></p>
          </div>
   
          <div>
            <p>Una app imprescindible para padres modernos.</p>
            <p><strong>Javier Pérez</strong></p>
          </div>
          
        </div>

      </div>

      <div className={containerComentarios}>
        <h2>¿Dudas o <span>comentarios</span>?</h2>
        <p>¿Tienes alguna pregunta o inquietud sobre cómo <span><strong>Zentinela</strong></span> puede <br /> cambiar la forma en que ves la movilidad escolar? <strong>¡Hablemos!</strong></p>

        <div>
          <img src="/images/whatsapp.svg" alt="" />
          <a href='https://api.whatsapp.com/send?phone=+51997947337&text=Necesito%20m%C3%A1s%20informaci%C3%B3n' target='__Blank'>Escríbenos... <span><img src="/icons/whatsapp.svg" alt="" /></span></a>
        </div>
      </div>

      <div className={containerFamilia}>
        <h2><span>Lo que piensa</span> nuestra familia</h2>

      </div>

      <div className={containerApps}>
        <h2>Es <span>más fácil</span> con las apps</h2>

        <div className={containerAppsCard}>
          <div>
            <div>
              <img src="/images/qr.svg" alt="" />
            </div>
            <div>
              <h4>Descarga la app de <span>Zentinela</span></h4>
              <p>Escanea para descargar</p>
            </div>
          </div>
          <div>
            <div>
              <img src="/images/qr.svg" alt="" />
            </div>
            <div>
              <h4>Descarga la app de <span>Zentinela <br /> para conductores</span></h4>
              <p>Escanea para descargar</p>
            </div>
          </div>
        </div>

      </div>

      <footer>
        <div className={logo}>
          <img src="/images/logo_text.svg" alt="" />
        </div>
        <div className={detail}>
          <div>
            <p>
              Ten control en tiempo real. Seguridad, flexibilidad en pagos y una experiencia personalizada.
              <br />
              <br />
              <strong>¡Descubre la tranquilidad ahora!</strong></p>
          </div>
          <div>
            <ul>
              <li>Compañia</li>
              <li><Link to={''}>Quiénes somos</Link></li>
              <li><Link to={''}>Lo que ofrecemos</Link></li>
            </ul>
            <ul>
              <li>Ciudadanía global</li>
              <li><Link to={''}>Seguridad</Link></li>
            </ul>
            <ul>
              <li>Viajes</li>
              <li><Link to={''}>Reservas</Link></li>
            </ul>
          </div>
        </div>

        <div className={social}>
          <ul>
            <li>
              <a href="">
                <AiOutlineWhatsApp />
              </a>
            </li>
            <li>
              <a href="">
                <FiInstagram />
              </a>
            </li>
            <li>
              <a href="">
                <BiLogoTiktok />
              </a>
            </li>
            <li>
              <a href="">
                <FaTwitch />
              </a>
            </li>
          </ul>
          <div className={socialActions}>
            <button>
              <img src="/icons/google_play.svg" alt="" />
            </button>
            <button>
              <img src="/icons/apple_store.svg" alt="" />
            </button>
          </div>
        </div>

        <div className={copy}>
          <p>© 2023 Zentinel Technologies, Inc.</p>

          <ul>
            <li>
              <Link to={''}>Privacidad</Link>
            </li>
            <li>
              <Link to={''}>Accesibilidad</Link>
            </li>
            <li>
              <Link to={''}>Términos</Link>
            </li>
          </ul>
        </div>
      </footer>

      <AnimatePresence>
        {isType.state && 
          <ModalState onAccept={() => toggleType(0)}>
            <div className={caracteristicas}>
              {DATA[isType.id - 1].map((card, index) => {
                return (
                  <div key={index}>
                    {card.left?.map((response, innerIndex) => {
                      const delay = 0.2 * innerIndex

                      return <motion.div
                        key={innerIndex}
                        initial={{ x: -20 }}
                        animate={{ x: 0 }}
                        transition={{ delay }}
                      >
                        <h5>{response.title} <span /></h5>
                        <p>{response.description}</p>
                      </motion.div>
                    })}
                    {card.rigth?.map((response, innerIndex) => {
                      const delay = 0.2 * innerIndex

                      return <motion.div
                        key={innerIndex}
                        initial={{ x: 20 }}
                        animate={{ x: 0 }}
                        transition={{ delay }}
                      >
                        <h5>{response.title} <span /></h5>
                        <p>{response.description}</p>
                      </motion.div>
                    })}
                  </div>
                );
              })}
            </div>
          </ModalState>
        }
      </AnimatePresence>
    </div>
  )
}

export default App
