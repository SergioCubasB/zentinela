import Styles from './ModalStateHead.module.scss'
import { IoClose } from 'react-icons/io5'

interface Props {
  onAccept: () => void
}

export const ModalStateHead = ({onAccept}: Props) => {
  const { container } = Styles

  return (
    <div className={container}>
      <button onClick={onAccept}><IoClose /></button>
    </div>
  )
}
