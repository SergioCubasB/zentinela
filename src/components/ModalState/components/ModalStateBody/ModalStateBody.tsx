import React from 'react'

import Styles from './ModalStateBody.module.scss'

interface Props {
  children: React.ReactNode
}

export const ModalStateBody = ({children}: Props) => {
  const { container } = Styles

  return (
    <div className={container}>
      {children}
    </div>
  )
}
