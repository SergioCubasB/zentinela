import { motion } from 'framer-motion'

import Styles from './ModalState.module.scss'

import { ModalStateHead } from './components/ModalStateHead/ModalStateHead'
import { ModalStateBody } from './components/ModalStateBody/ModalStateBody'
import { useWindowSize } from '../../hooks/useWindowSize'
import { useEffect } from 'react'

interface Props {
  onAccept: () => void
  children: React.ReactNode 
}

export const ModalState = ({onAccept, children}: Props) => {
  const { container, containerModal, containerOverlay } = Styles
  const {width} = useWindowSize()

  const isMobile = () => {
    return width && width <= 768
  }

  const motionConfig = { 
    initial : {y: isMobile() ? -100 : 100, opacity: .5 }, 
    animate : { y: 0, opacity: 1 }, 
    exit    : { y: isMobile() ? 100 : -100, opacity: 0 }
  }

  useEffect(() => {
    document.body.style.overflow = 'hidden';
  
    return () => {
      document.body.style.overflow = 'auto';
    }
  }, [])
  

  return (
    <div className={container}>
        <motion.div
          className={containerModal}
          initial = {motionConfig?.initial}
          animate = {motionConfig?.animate}
          exit    = {motionConfig?.exit}>
          <ModalStateHead onAccept={() => onAccept()} />
          <ModalStateBody>
            {children}
          </ModalStateBody>
      </motion.div>
      <div className={containerOverlay} />
    </div>
  )
}
