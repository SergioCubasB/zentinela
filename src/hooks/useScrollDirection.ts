import { useEffect, useState } from 'react';

interface ScrollDirection {
  isScrollingDown: boolean;
  isScrollingUp: boolean;
}

const useScrollDirection = (): ScrollDirection => {
  const [scrollDirection, setScrollDirection] = useState<ScrollDirection>({
    isScrollingDown: false,
    isScrollingUp: false,
  });

  useEffect(() => {
    let lastScrollY = window.scrollY;

    const handleScroll = () => {
      const currentScrollY = window.scrollY;

      setScrollDirection((prevDirection) => {
        if (currentScrollY > lastScrollY) {
          return {
            isScrollingDown: true,
            isScrollingUp: false,
          };
        } else if (currentScrollY < lastScrollY) {
          return {
            isScrollingDown: false,
            isScrollingUp: true,
          };
        } else {
          return prevDirection;
        }
      });

      lastScrollY = currentScrollY;
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return scrollDirection;
};

export default useScrollDirection;
